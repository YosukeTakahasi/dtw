import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class DTW {
	protected double[] seq1;
	protected double[] seq2;
	protected int[][] warpingPath;

	protected int n;
	protected int m;
	protected int K;

	protected double warpingDistance;

	public DTW(double[] sample, double[] templete) {
		seq1 = sample;
		seq2 = templete;

		n = seq1.length;
		m = seq2.length;
		K = 1;
		warpingPath = new int[n + m][2]; // max(n, m) <= K < n + m
		warpingDistance = 0.0;

		this.compute();
	}

	public void compute() {
		double accumulatedDistance = 0.0; // 蓄積された距離

		double[][] d = new double[n][m]; // local distances
		double[][] D = new double[n][m]; // global distances

		//テンプレートデータと認証データの距離を総当たりで計算
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				d[i][j] = distanceBetween(seq1[i], seq2[j]);
			}
		}

		D[0][0] = d[0][0];
		// 格子のx軸方向で一つ前と後で比較
		for (int i = 1; i < n; i++) {
			D[i][0] = d[i][0] + D[i - 1][0];
		}
		// 格子のy軸方向で一つ前と後で比較
		for (int j = 1; j < m; j++) {
			D[0][j] = d[0][j] + D[0][j - 1];
		}

		for (int i = 1; i < n; i++) {
			for (int j = 1; j < m; j++) {
				accumulatedDistance = Math.min(
						Math.min(D[i - 1][j], D[i - 1][j - 1]), D[i][j - 1]);
				accumulatedDistance += d[i][j];
				D[i][j] = accumulatedDistance;
			}
		}
		accumulatedDistance = D[n - 1][m - 1];

		int i = n - 1;
		int j = m - 1;
		int minIndex = 1;

		warpingPath[K - 1][0] = i;
		warpingPath[K - 1][1] = j;

		while ((i + j) != 0) {
			if (i == 0) {
				j -= 1;
			} else if (j == 0) {
				i -= 1;
			} else { // i != 0 && j != 0
				double[] array = { D[i - 1][j], D[i][j - 1], D[i - 1][j - 1] };
				minIndex = this.getIndexOfMinimum(array);

				if (minIndex == 0) {
					i -= 1;
				} else if (minIndex == 1) {
					j -= 1;
				} else if (minIndex == 2) {
					i -= 1;
					j -= 1;
				}
			} // end else
			K++;
			warpingPath[K - 1][0] = i;
			warpingPath[K - 1][1] = j;
		} // end while
		warpingDistance = accumulatedDistance / K;

		this.reversePath(warpingPath);
	}

	protected void reversePath(int[][] path) {
		int[][] newPath = new int[K][2];
		for (int i = 0; i < K; i++) {
			for (int j = 0; j < 2; j++) {
				newPath[i][j] = path[K - i - 1][j];
			}
		}
		warpingPath = newPath;
	}
	public double getDistance() {
		return warpingDistance;
	}
	protected double distanceBetween(double p1, double p2) {
		return (p1 - p2) * (p1 - p2);
	}

	protected int getIndexOfMinimum(double[] array) {
		int index = 0;
		double val = array[0];

		for (int i = 1; i < array.length; i++) {
			if (array[i] < val) {
				val = array[i];
				index = i;
			}
		}
		return index;
	}
    public String toString() {
        String retVal = "Warping Distance: " + warpingDistance + "\n";
		retVal += "Warping Path: {";
		for (int i = 0; i < K; i++) {
			retVal += "(" + warpingPath[i][0] + ", " +warpingPath[i][1] + ")";
			retVal += (i == K - 1) ? "}" : ", ";
	    }
		return retVal;
    }
	public static String[][] getCSV_All(String myFILE) {
		// ファイルデータ：返り値用2次元配列
		String[][] charaDataArray = { { "カラ" } };
		// ファイルデータ：一時保存用リスト
		ArrayList<String[]> lists = new ArrayList<String[]>();

		try {
			// ファイルの読み込み準備
			File csv = new File(myFILE);
			BufferedReader br = new BufferedReader(new FileReader(csv));

			// データをリストに保存
			String line = null;
			while ((line = br.readLine()) != null) {
				// 読み込んだ文字列 line をカンマで分割して lists に格納
				lists.add(line.split(","));
			}
			br.close();
			// リストから2次元配列に移動
			charaDataArray = new String[lists.size()][];
			for (int i = 0; i < lists.size(); i++) {
				charaDataArray[i] = lists.get(i);
			}
		} catch (FileNotFoundException e) {
			// Fileオブジェクト生成時の例外捕捉
			e.printStackTrace();
			System.out.println("ファイルが見つかりませんでした");
		} catch (IOException e) {
			// BufferedReaderオブジェクトのクローズ時の例外捕捉
			e.printStackTrace();
			System.out.println("IOエラー");
		}
		return charaDataArray;
	}

	public static void main(String[] args) {
		String path = "./sample.csv";
		String CSV_Sample1[][] = getCSV_All(path);

		// CSVファイルをdouble型に変換
		double data[][] = new double[CSV_Sample1.length][];
		for (int i = 0; i < CSV_Sample1.length; i++) {
			data[i] = new double[CSV_Sample1[i].length];
			for (int j = 0; j < CSV_Sample1[i].length; j++) {
				// data[i][j] = Double.parseDouble(CSV_Sample1[i][j]);
				data[i][j] = Double.parseDouble(CSV_Sample1[i][j]);
			}
		}

		//CSVファイルの行数を取ってくる
		LineCount linecount = new LineCount();
		int count = linecount.CountNumberOfTextLines("./sample.csv");

		for (int i = 0; i<count; i++){
			double[] n1 = data[0];
			double[] n2 = data[i];
			DTW dtw = new DTW(n1,n2);
			// DTWスコアを出力
			System.out.println(dtw);
		}
	}
}